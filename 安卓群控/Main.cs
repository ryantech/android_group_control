﻿using adb群控.service;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace adb群控
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 定义异步刷新事件，刷新设备列表
        /// </summary>
        private delegate void AsynreullDeviceData();


        /// <summary>
        /// 异步事件
        /// </summary>
        private delegate void AsynEvent();

        public Dictionary<String, Form> forms = new Dictionary<string, Form>();


        private List<model.Device> list = null;

        private void Main_Load(object sender, EventArgs e)
        {
            Service service = new Service();
            //service.sendMessageToAdbRec("host:devices");
            //service.closeProcess();
            AsynreullDeviceData asynreullDeviceData = new AsynreullDeviceData(reullDeviceData);
            asynreullDeviceData.BeginInvoke(null, null);
            lab_name.Text = "";
            lab_cpu.Text = "";
            lab_sdk.Text = "";
            lab_pmcd.Text = "";
            lab_pmkd.Text = "";
            lab_port.Text = "";
            lab_status.Text = "";


        }

        /// <summary>
        /// 刷新data
        /// </summary>
        private void reullDeviceData()
        {
            try
            {
                this.Invoke(new AsynreullDeviceData(delegate ()
                {
                    button1.Enabled = false;
                    button1.Text = "列表正在加载";
                    this.pritlnLog("正在获取连接设备请内心等待");
                }));
                Service main = new Service();
                List<model.Device> list = main.getAllDevice();
                this.list = list;
                this.Invoke(new delegateRefullData(delegate ()
                {
                    if (list != null)
                    {
                        dataGridView1.DataSource = this.getRefull(list);
                        button1.Enabled = true;
                        button1.Text = "刷新设备列表";
                        this.pritlnLog("设备获取完成，如果有新设备请点击“刷新设备”按钮");
                    }

                }));
                Debug.WriteLine("执行设备刷新操作");
                Thread.Sleep(1000 * 10);//设置刷新时长
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        //刷新委托
        private delegate void delegateRefullData();
        private DataTable getRefull(List<model.Device> list)
        {
            DataTable dt = new DataTable();
            DataColumn dc1 = new DataColumn("设备名称");
            DataColumn dc2 = new DataColumn("cpu型号");
            DataColumn dc3 = new DataColumn("sdk版本");
            DataColumn dc4 = new DataColumn("屏幕长度");
            DataColumn dc5 = new DataColumn("屏幕宽度");
            DataColumn dc6 = new DataColumn("设备对应对口");
            DataColumn dc7 = new DataColumn("设备状态");
            dt.Columns.Add(dc1);
            dt.Columns.Add(dc2);
            dt.Columns.Add(dc3);
            dt.Columns.Add(dc4);
            dt.Columns.Add(dc5);
            dt.Columns.Add(dc6);
            dt.Columns.Add(dc7);
            Service service = new Service();
            if (list == null)
            {
                return dt;
            }
            foreach (var d in list)
            {
                DataRow dr = dt.NewRow();
                dr[0] = d.Name;
                dr[1] = d.CupAbi;
                dr[2] = d.SdkVersion;
                dr[3] = d.Higth;
                dr[4] = d.Width;
                dr[5] = d.Port;
                dr[6] = d.Status;
                dt.Rows.Add(dr);
            }
            return dt;
        }

        private void dataGridView1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {

            try
            {
                lab_name.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString(); ;
                lab_cpu.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString(); ;
                lab_sdk.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
                lab_pmcd.Text = dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString();
                lab_pmkd.Text = dataGridView1.Rows[e.RowIndex].Cells[4].Value.ToString();
                lab_port.Text = dataGridView1.Rows[e.RowIndex].Cells[5].Value.ToString();
                lab_status.Text = dataGridView1.Rows[e.RowIndex].Cells[6].Value.ToString();
            }
            catch (Exception)
            {


            }
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Service service = new Service();
            //service.closeProcess();
            System.Environment.Exit(0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AsynreullDeviceData asynreullDeviceData = new AsynreullDeviceData(reullDeviceData);
            asynreullDeviceData.BeginInvoke(null, null);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("初始化过的设备请不要重复初始化", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.Cancel)
            {
                if (list == null || list.Count <= 0)
                {
                    MessageBox.Show("没有要初始的设备，刷新设备列表");
                    return;
                }
                AsynEvent AsynEvent = new AsynEvent(delegate ()
                {

                    this.Invoke(new delegateRefullData(delegate ()
                    {
                        this.button2.Enabled = false;
                        button2.Text = "设备正在初始化";

                    }));

                    foreach (model.Device device in list)
                    {
                        Service service = new Service();
                        this.Invoke(new delegateRefullData(delegate ()
                        {
                            string info = device.Name + "：" + "请在初始化......";
                            pritlnLog(info);

                        }));
                        String result = service.initDevices(device);
                        this.Invoke(new delegateRefullData(delegate ()
                        {
                            string info = device.Name + "：初始化完成：" + result;
                            pritlnLog(info);

                        }));
                    }
                    this.Invoke(new delegateRefullData(delegate ()
                    {
                        this.button2.Enabled = true;
                        button2.Text = "全部设备初始化";

                    }));
                });
                AsynEvent.BeginInvoke(null, null);
            }


        }

        /// <summary>
        /// 页面输出日志
        /// </summary>
        /// <param name="info"></param>
        private void pritlnLog(String info)
        {
            string logInfo = txt_log.Text + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + ":" + info + Environment.NewLine;
            txt_log.Text = logInfo;
            txt_log.SelectionStart = txt_log.Text.Length;
            txt_log.SelectionLength = 0;
            txt_log.ScrollToCaret();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (lab_name.Text.Length == 0 || lab_name.Text == "" || lab_name.Text.Equals(""))
            {
                MessageBox.Show("请选择要打开的设备");
                return;
            }
            model.Device d = new model.Device();
            d.Name = lab_name.Text;
            d.CupAbi = lab_cpu.Text;
            d.SdkVersion = lab_sdk.Text;
            d.Higth = lab_pmcd.Text;
            d.Width = lab_pmkd.Text;
            d.Port = Convert.ToInt32(lab_port.Text);
            Device form1 = new Device(d);
            form1.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (lab_name.Text.Length == 0 || lab_name.Text == "" || lab_name.Text.Equals(""))
            {
                MessageBox.Show("请选择要打开的设备");
                return;
            }
            if (MessageBox.Show("初始化过的设备请不要重复初始化", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) != DialogResult.Cancel)
            {
                if (list == null || list.Count <= 0)
                {
                    MessageBox.Show("没有要初始的设备，刷新设备列表");
                    return;
                }
                AsynEvent AsynEvent = new AsynEvent(delegate ()
                {

                    this.Invoke(new delegateRefullData(delegate ()
                    {
                        this.button4.Enabled = false;
                        button4.Text = "设备正在初始化";

                    }));

                    model.Device device = new model.Device();
                    device.Name = lab_name.Text;
                    device.CupAbi = lab_cpu.Text;
                    device.SdkVersion = lab_sdk.Text;
                    device.Higth = lab_pmcd.Text;
                    device.Width = lab_pmkd.Text;
                    device.Port = Convert.ToInt32(lab_port.Text);
                    Service service = new Service();
                    this.Invoke(new delegateRefullData(delegate ()
                    {
                        string info = device.Name + "：" + "请在初始化......";
                        pritlnLog(info);

                    }));
                    String result = service.initDevices(device);
                    this.Invoke(new delegateRefullData(delegate ()
                    {
                        string info = device.Name + "：初始化完成：" + result;
                        pritlnLog(info);

                    }));
                    this.Invoke(new delegateRefullData(delegate ()
                    {
                        this.button4.Enabled = true;
                        button4.Text = "设备初始化";

                    }));
                });
                AsynEvent.BeginInvoke(null, null);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (lab_name.Text.Length == 0 || lab_name.Text == "" || lab_name.Text.Equals(""))
            {
                MessageBox.Show("请选择要打开的设备");
                return;
            }
            AsynEvent AsynEvent = new AsynEvent(delegate () {
                Service service = new Service();
                service.deviceReBoot(lab_name.Text);

            });
            AsynEvent.BeginInvoke(null,null);


        }

       
    }
}
